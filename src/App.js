import styles from "./App.module.css";
import {
    BrowserRouter as Router, Routes, Route, Link, Navigate,
} from "react-router-dom";
import {HarryPotter} from "./HarryPotter";
import {RickAndMorty} from "./RickAndMorty";
import {Menu} from "./Menu";

function App() {
    return (
        <Router>
            <div className={styles.container}>
                <header className={styles.container__header}>
                    <h1 className={styles.container__header__title}>Personajes</h1>

                </header>
                <main>
                    <div>
                        <Routes>
                            <Route path="/harrypotter" element={<HarryPotter/>}/>
                            <Route path="/rickandmorty" element={<RickAndMorty/>}/>
                            <Route path="/" element={<Menu/>}/>
                        </Routes>
                    </div>

                </main>
            </div>
        </Router>
    );
}

export default App;
