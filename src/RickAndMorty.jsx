import {Link} from "react-router-dom"
import ApiRickAndM from "./ApiRickAndM";
import styles from "./Api.module.css"

export function RickAndMorty() {
    return (
        <div>
            <div>
                <Link to={"/"}>
                    <h3 className={styles.home} >Home</h3>
                </Link>
            </div>
            <div>
                <h2 className={styles.title}> Personajes de Rick and Morty</h2>
            </div>
            <div>
                <ApiRickAndM/>
            </div>

        </div>


    )

}