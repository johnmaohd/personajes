import styles from "./Api.module.css"

const CharacterList = ({characters}) => {
    return (<div className={styles.card}>
        {characters.map((item, index) => (<div className={styles.container} key={index}>
            <div>
                <div className={styles.back}>
                    <strong><p className={styles.card__name}>{item.name}</p></strong>
                    <figure className={styles.card__imgContent}>
                        <img width={150} className={styles.card__image} src={item.image} alt="character"/>
                    </figure>
                </div >
                <div className={styles.card__backInfo}>
                    <div className={styles.card__backInfo}>

                        <div className={styles.card__info}>
                            <p><strong>Species: </strong>{item.species}</p>
                            <p><strong>Status: </strong>{item.status}</p>
                            <p><strong>Gender: </strong>{item.gender}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>))}

    </div>);
};

export default CharacterList;