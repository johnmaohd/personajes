import {Link} from "react-router-dom"
import ApiHarry from "./ApiHarry";
import styles from "./Api.module.css"

export function HarryPotter() {
    return (
        <div>

            <div>
                <Link to={"/"}>
                    <h3 className={styles.home}>Home</h3>
                </Link>
            </div>
            <div>
                <h2 className={styles.title}>Personajes de Harry Potter</h2>
            </div>
            <div>
                <ApiHarry/>
            </div>

        </div>

    )
}