import { useState, useEffect } from 'react';
import styles from "./Api.module.css"

function ApiHarry() {

    const [harry, setHarry] = useState([])
    const url = 'http://hp-api.herokuapp.com/api/characters'

    useEffect(() => {
        fetch(url)
            .then((response) => {
                return response.json()
            })
            .then((harry) => {
                setHarry(harry)
            })
    }, [])

    return (
        <div>
            <div className={styles.card}>

                {harry.map(harry => {
                    return (
                        <div className={styles.container}>
                        <div key={harry.image}>
                            <p className={styles.card__name}><strong>{harry.name}</strong></p>
                            <div className={styles.card__imgContent}>

                                <img
                                    width={150}
                                    className={styles.card__image}
                                    src={harry.image}
                                    alt={harry.name} />

                            </div>
                            <div className={styles.card__info}>
                                <p><strong> Specie: </strong>Specie: {harry.species}</p>
                                <p><strong>Gender: </strong>{harry.gender}</p>
                                <p><strong>House: </strong>{harry.house}</p>
                                <p><strong>Eye colour: </strong>{harry.eyeColour}</p>
                            </div>

                        </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default ApiHarry