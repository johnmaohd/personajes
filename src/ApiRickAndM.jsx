import CharacterList from "./CharacterList";
import {useEffect, useState} from "react";
import axios from "axios";
function ApiRickAndM() {
    const [characters, setCharacters] = useState([]);
    const url = "https://rickandmortyapi.com/api/character";

    const fetchCharacters = (url) => {
        axios
            .get(url)
            .then((data) => {
                setCharacters(data.data.results);
            })
            .catch((error) => {
                console.log(error);
            });
    };
    useEffect(() => {
        fetchCharacters(url);
    }, []);
    return (
        <div className='container'>
            <CharacterList characters={characters} />
        </div>
    );
    return (
        <div className='container'>
            <CharacterList characters={characters} />
        </div>

    )


}

export default ApiRickAndM