import {Link} from "react-router-dom";
import styles from "./Menu.module.css";
export function Menu() {
    return (
        <div className={styles.menu}>
            <ul className={styles.menu__list}>
                <li className={styles.menu__list__items}>
                    <Link to={"/"}>
                        <h3 className={styles.home_text}>Home</h3>
                    </Link>
                </li>
                <li className={styles.menu__list__items}>
                    <button className={styles.menu__list__button}>
                        <Link to={"/harrypotter"}>
                            <h3>Harry Potter</h3>
                        </Link>
                    </button>
                </li>
                <li className={styles.menu__list__items}>
                    <button className={styles.menu__list__button}>
                        <Link to={"/rickandmorty"}>
                            <h3>Rick and Morty</h3>
                        </Link>
                    </button>

                </li>

            </ul>
        </div>


    )

}